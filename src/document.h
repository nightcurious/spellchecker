#include <stdbool.h>

typedef struct Document_t *Document;

/*
doc_open
    Inicializador Document
    fname : path al archivo
*/
Document doc_open(char *src, char *dst);

/*
doc_get_word
    Obtiene la próxima palabra.
    d : Document source
    word : array donde almacenar obtenido
*/
bool doc_get_word(Document d, char *word);

/*
doc_put_word
    Escribe una palabra en Document
    d : Document source
    word : array con contenido a escribir en 'Document d'
*/
void doc_put_word(Document d, char *word);

/*
doc_close
    Encargado de acciones de limpieza y cerrar archivos.
    d : Document source
*/
void doc_close(Document d);
