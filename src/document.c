#include "document.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <locale.h>
#include <wchar.h>


struct Document_t
{
    FILE *doc_in;
    FILE *doc_out;
};

Document doc_open(char *src, char *dst){
    assert(src != NULL && dst != NULL);
    Document result = NULL;

    result = malloc(sizeof(struct Document_t));
    assert(result != NULL);
    result->doc_in = fopen(src, "r");
    result->doc_out = fopen(dst, "w");
    return result;
}

void doc_close(Document d){
    assert(d != NULL);
    fclose(d->doc_in);
    fclose(d->doc_out);
    free(d);
}

bool doc_get_word(Document d, char *word){
    assert(d != NULL);
    assert(word != NULL);
    int i = 0, h = 0;
    char c;

    while((c = getc(d->doc_in)) != EOF){
        if(isalpha(c)){
            word[i] = c;
            h++;
            i++;
        }else{
            if(h == 0){
                doc_put_word(d, &c);
            }else{
                word[i] = '\0';
                fseek(d->doc_in, -1, SEEK_CUR);
                return true;
            }
        }
    }
    return false;
}

void doc_put_word(Document d, char *word){
    assert(d != NULL);
    assert(word != NULL);
    fputs(word, d->doc_out);
}
