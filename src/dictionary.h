
typedef struct Dictionary_t *Dictionary;

//tamaño maximo de una palabra
#define MAX_WORD_SIZE 30

Dictionary dict_new(void);
Dictionary dict_load(Dictionary dict, char *fname);
Dictionary dict_add(Dictionary dict, char *word);
bool dict_contains(Dictionary dictionary, char *word);
void dict_save(Dictionary dictionary, char *fname);
void dict_destroy(Dictionary dictionary);
