#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <assert.h>
#include "dictionary.h"

struct Dictionary_t {
    int size;
    char **dict;
};

Dictionary dict_new(void){
    Dictionary result = NULL;
    result = malloc(sizeof(struct Dictionary_t));
    assert(result != NULL);
    result->size = 0;
    result->dict = NULL;
    return result;
}

Dictionary dict_load(Dictionary dict, char *fname){
    char word[MAX_WORD_SIZE];
    int word_length, i = 0;
    char *remove;
    FILE *fp;

    fp = fopen(fname, "r");

    while(fgets(word, MAX_WORD_SIZE, fp) != NULL){
        word_length = strlen(word);
        if((remove = strchr(word, '\n')) != NULL){
            if(*(remove - 1) == '\r')
                word_length -= 2;
            else
                word_length--;
            word[word_length] = '\0';
        }else{
            while(getc(fp) != '\n');
        }
        if(strlen(word) > 0)
            dict = dict_add(dict, word);
        assert(dict != NULL);
    }
    fclose(fp);
    return dict;
};

Dictionary dict_add(Dictionary dictionary, char *word){
    if(dictionary->size == 0){
        dictionary->dict = malloc(sizeof(char *));
        assert(dictionary->dict != NULL);
    }else{
        dictionary->dict = realloc(dictionary->dict, sizeof(char *) * (dictionary->size + 1));
        assert(dictionary->dict != NULL);
    }
    *(dictionary->dict + dictionary->size) = malloc(sizeof(char) * (strlen(word) + 1));
    assert(dictionary->dict + dictionary->size != NULL);
    strcpy(*(dictionary->dict + dictionary->size), word);
    dictionary->size++;
    return dictionary;
};

bool dict_contains(Dictionary dictionary, char *word){
    int i;

    for(i = 0; i < dictionary->size; i++){
        if(strcmp(*(dictionary->dict + i), word) == 0)
            return true;
    }
    return false;
}

void dict_save(Dictionary dictionary, char *fname){
    int i = 0;
    FILE *fp;

    fp = fopen(fname, "w");
    while(i < dictionary->size){
        fprintf(fp, "%s\n", *(dictionary->dict + i));
        i++;
    }
    fclose(fp);
}


void dict_destroy(Dictionary dictionary){
    int i;

    if(dictionary != NULL) {
        for(i = 0; i < dictionary->size; i++)
            free(dictionary->dict[i]);
        free(dictionary->dict);
        free(dictionary);
        dictionary = NULL;
    }
}
